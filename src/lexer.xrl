Definitions.

I = [0-9](\_?[0-9])*

U = [a-zA-Z_]+([a-zA-Z0-9_@])*(!|\?)?

S = [A-Z]+[a-zA-Z0-9_]*

O = (@|\.(\.)?|\+(\+)?(\+)?|-(-)?(-)?|\!(=)?(=)?|\^(\^\^)?|~~~|\*|/|<>|\|>|<<<|>>>|<<~|~>>|<~|~>|<~>|<\|>|<|>|<=|>=|=(=)?(=)?|=~|&(&)?(&)?|\|(\|)?(\|)?|::|<-|\\|,|\%)

M = """

C = '''

Rules.

\s|\n|\t|\r
: {token, {space, TokenLine, TokenChars}}.

"([^"]|\\")*"
: {token, {string, TokenLine, TokenChars}}.

"""([^{M}]|\s|\n|\t|\r)*"""
: {token, {multilineString, TokenLine, TokenChars}}.

'([^']|\\')*'
: {token, {charlist, TokenLine, TokenChars}}.

'''([^{C}]|\s|\n|\t|\r)*'''
: {token, {multilineCharlist, TokenLine, TokenChars}}.

true|false|nil|use
: {token, {reservedAtomWord, TokenLine, TokenChars}}.

__MODULE__|__ENV__|__DIR__|__STACKTRACE__|__CALLER__
: {token, {kernelSpecialForm, TokenLine, TokenChars}}.

when|and|or|not|in|fn|do|end|catch|rescue|after|if|else|def|defmodule|defstruct|defp|defexception|defprotocol|defimpl|defmacro|receive|try|cond 
: {token, {reservedWord, TokenLine, TokenChars}}.

{I}
: {token, {integer, TokenLine, TokenChars}}.

{I}\.{I}((E|e)(\+|-)?{I})? 
: {token, {float, TokenLine, TokenChars}}.

:{U}|:["](.)+["]|:{O}|{U}:
: {token, {atom, TokenLine, TokenChars}}.

{S}
: {token, {uppercaseAtom, TokenLine, TokenChars}}.

\(|\)|\[|\]|\{|\}|<<|>>
: {token, {delimiter, TokenLine, TokenChars}}.

{O}|=>
: {token, {operator, TokenLine, TokenChars}}.

[a-z]+[a-zA-Z0-9_]*(!|\?)?
: {token, {variable, TokenLine, TokenChars}}.

_[a-zA-Z0-9_]*(!|\?)?
: {token, {unusedVariable, TokenLine, TokenChars}}.

#(.)*
: {token, {comment, TokenLine, TokenChars}}.

defmodule(\s)*{S}(\.{S})*
: {token, {moduleName, TokenLine, TokenChars}}.

def(\s)*[_a-z]+[a-zA-Z0-9_]*(!|\?)?
: {token, {functionName, TokenLine, TokenChars}}.

(.)
: {token, {theRest, TokenLine, TokenChars}}.

Erlang code.