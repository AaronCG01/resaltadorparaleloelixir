defmodule Resaltador.Paralelo do

  def openElixirFile(elixirFileName) do
    {:ok, elixirFile} = File.open("elixirCodes/" <> elixirFileName, [:read, :utf8])
    elixirFile
  end

  def lexicalAnalysis(inputFile) do
    {_, tokensList, _} =
      IO.read(inputFile, :all)
      |> String.to_charlist()
      |> :lexer.string()
    tokensList
  end

  def openHtmlFile(htmlFileName) do
    {:ok, htmlFile} =
      File.open("htmlFiles/" <> String.replace_suffix(htmlFileName, ".ex", ".html"), [:write])
    htmlFile
  end

  def linkHtmlWithCss(htmlFile) do
    fixedCode =
      "<html>\n<head>\n<link rel=\"stylesheet\" href=\"../styles.css\">\n</head>\n<body>\n<pre>\n<code>\n"
    IO.write(htmlFile, fixedCode)
    htmlFile
  end

  def writeTokensInHtml(htmlFile, [{:reservedAtomWord, _, text} | tail]) do
    IO.write(htmlFile, "<span class=\"reservedAtomWord\">" <> to_string(text) <> "</span>")
    writeTokensInHtml(htmlFile, tail)
  end

  def writeTokensInHtml(htmlFile, [{:kernelSpecialForm, _, text} | tail]) do
    IO.write(htmlFile, "<span class=\"kernelSpecialForm\">" <> to_string(text) <> "</span>")
    writeTokensInHtml(htmlFile, tail)
  end

  def writeTokensInHtml(htmlFile, [{:reservedWord, _, text} | tail]) do
    IO.write(htmlFile, "<span class=\"reservedWord\">" <> to_string(text) <> "</span>")
    writeTokensInHtml(htmlFile, tail)
  end

  def writeTokensInHtml(htmlFile, [{:integer, _, text} | tail]) do
    IO.write(htmlFile, "<span class=\"integer\">" <> to_string(text) <> "</span>")
    writeTokensInHtml(htmlFile, tail)
  end

  def writeTokensInHtml(htmlFile, [{:float, _, text} | tail]) do
    IO.write(htmlFile, "<span class=\"float\">" <> to_string(text) <> "</span>")
    writeTokensInHtml(htmlFile, tail)
  end

  def writeTokensInHtml(htmlFile, [{:atom, _, text} | tail]) do
    IO.write(htmlFile, "<span class=\"atom\">" <> to_string(text) <> "</span>")
    writeTokensInHtml(htmlFile, tail)
  end

  def writeTokensInHtml(htmlFile, [{:uppercaseAtom, _, text} | tail]) do
    IO.write(htmlFile, "<span class=\"uppercaseAtom\">" <> to_string(text) <> "</span>")
    writeTokensInHtml(htmlFile, tail)
  end

  def writeTokensInHtml(htmlFile, [{:delimiter, _, text} | tail]) do
    IO.write(htmlFile, "<span class=\"delimiter\">" <> to_string(text) <> "</span>")
    writeTokensInHtml(htmlFile, tail)
  end

  def writeTokensInHtml(htmlFile, [{:unusedVariable, _, text} | tail]) do
    IO.write(htmlFile, "<span class=\"unusedVariable\">" <> to_string(text) <> "</span>")
    writeTokensInHtml(htmlFile, tail)
  end

  def writeTokensInHtml(htmlFile, [{:comment, _, text} | tail]) do
    IO.write(
      htmlFile,
      "<span class=\"comment\">" <> HtmlEntities.encode(to_string(text)) <> "</span>"
    )
    writeTokensInHtml(htmlFile, tail)
  end

  def writeTokensInHtml(htmlFile, [{:moduleName, _, text} | tail]) do
    IO.write(
      htmlFile,
      "<span class=\"reservedWord\">defmodule</span><span class=\"moduleName\">" <>
        to_string(
          String.codepoints(to_string(text)) -- ["d", "e", "f", "m", "o", "d", "u", "l", "e"]
        ) <> "</span>"
    )
    writeTokensInHtml(htmlFile, tail)
  end

  def writeTokensInHtml(htmlFile, [{:functionName, _, text} | tail]) do
    IO.write(
      htmlFile,
      "<span class=\"reservedWord\">def</span><span class=\"functionName\">" <>
        to_string(String.codepoints(to_string(text)) -- ["d", "e", "f"]) <> "</span>"
    )
    writeTokensInHtml(htmlFile, tail)
  end

  def writeTokensInHtml(htmlFile, [{:string, _, text} | tail]) do
    IO.write(htmlFile, "<span class=\"string\">" <> HtmlEntities.encode(to_string(text)) <> "</span>")
    writeTokensInHtml(htmlFile, tail)
  end

  def writeTokensInHtml(htmlFile, [{:multilineString, _, text} | tail]) do
    IO.write(
      htmlFile,
      "<span class=\"multilineString\">" <> HtmlEntities.encode(to_string(text)) <> "</span>"
    )
    writeTokensInHtml(htmlFile, tail)
  end

  def writeTokensInHtml(htmlFile, [{:charlist, _, text} | tail]) do
    IO.write(
      htmlFile,
      "<span class=\"charlist\">" <> HtmlEntities.encode(to_string(text)) <> "</span>"
    )
    writeTokensInHtml(htmlFile, tail)
  end

  def writeTokensInHtml(htmlFile, [{:multilineCharlist, _, text} | tail]) do
    IO.write(
      htmlFile,
      "<span class=\"multilineCharlist\">" <> HtmlEntities.encode(to_string(text)) <> "</span>"
    )
    writeTokensInHtml(htmlFile, tail)
  end

  def writeTokensInHtml(htmlFile, [{_, _, text} | tail]) do
    IO.write(
      htmlFile,
      "<span class=\"generic\">" <> HtmlEntities.encode(to_string(text)) <> "</span>"
    )
    writeTokensInHtml(htmlFile, tail)
  end

  def writeTokensInHtml(htmlFile, []) do
    IO.write(htmlFile, "</code>\n</pre>\n</body>\n</html>")
    File.close(htmlFile)
  end

  def main(elixirFileName) do
    tokensList = openElixirFile(elixirFileName)
    |> lexicalAnalysis
    openHtmlFile(elixirFileName)
    |> linkHtmlWithCss
    |> writeTokensInHtml(tokensList)
  end

  def sequential(folderName \\ "elixirCodes") do
    {:ok, elixirFiles} = File.ls(folderName)
    Enum.map(elixirFiles, fn elixirFile -> main(elixirFile) end)
  end

  def concurrent(folderName \\ "elixirCodes") do
    {:ok, elixirFiles} = File.ls(folderName)
    Enum.map(elixirFiles, fn elixirFile -> spawn(fn -> main(elixirFile) end) end)
  end

  def benchmark do
    Benchee.run(%{
      "seq" => fn -> sequential() end,
      "conc" => fn -> concurrent() end
    })
  end

end
