<html>
<head>
<link rel="stylesheet" href="../styles.css">
</head>
<body>
<pre>
<code>
<span class="reservedWord">defmodule</span><span class="moduleName"> GenEvent</span><span class="generic"> </span><span class="reservedWord">do</span><span class="generic">
</span><span class="generic"> </span><span class="generic"> </span><span class="generic">@</span><span class="generic">moduledoc</span><span class="generic"> </span><span class="string">&quot;&quot;</span><span class="string">&quot;
  A behaviour module for implementing event handling functionality.

  The event handling model consists of a generic event manager
  process with an arbitrary number of event handlers which are
  added and deleted dynamically.

  An event manager implemented using this module will have a standard
  set of interface functions and include functionality for tracing and
  error reporting. It will also fit into a supervision tree.

  ## Example

  There are many use cases for event handlers. For example, a logging
  system can be built using event handlers where each log message is
  an event and different event handlers can be plugged to handle the
  log messages. One handler may print error messages on the terminal,
  another can write it to a file, while a third one can keep the
  messages in memory (like a buffer) until they are read.

  As an example, let&#39;s have a GenEvent that accumulates messages until
  they are collected by an explicit call.

      defmodule LoggerHandler do
        use GenEvent

        # Callbacks

        def handle_event({:log, x}, messages) do
          {:ok, [x|messages]}
        end

        def handle_call(:messages, messages) do
          {:ok, Enum.reverse(messages), []}
        end
      end

      {:ok, pid} = GenEvent.start_link()

      GenEvent.add_handler(pid, LoggerHandler, [])
      #=&gt; :ok

      GenEvent.notify(pid, {:log, 1})
      #=&gt; :ok

      GenEvent.notify(pid, {:log, 2})
      #=&gt; :ok

      GenEvent.call(pid, LoggerHandler, :messages)
      #=&gt; [1, 2]

      GenEvent.call(pid, LoggerHandler, :messages)
      #=&gt; []

  We start a new event manager by calling `GenEvent.start_link/0`.
  Notifications can be sent to the event manager which will then
  invoke `handle_event/2` for each registered handler.

  We can add new handlers with `add_handler/3` and `add_mon_handler/3`.
  Calls can also be made to specific handlers by using `call/3`.

  ## Callbacks

  There are 6 callbacks required to be implemented in a `GenEvent`. By
  adding `use GenEvent` to your module, Elixir will automatically define
  all 6 callbacks for you, leaving it up to you to implement the ones
  you want to customize. The callbacks are:

    * `init(args)` - invoked when the event handler is added.

      It must return:

      -  `{:ok, state}`
      -  `{:ok, state, :hibernate}`
      -  `{:error, reason}`

    * `handle_event(msg, state)` - invoked whenever an event is sent via
      `notify/2`, `ack_notify/2` or `sync_notify/2`.

      It must return:

      -  `{:ok, new_state}`
      -  `{:ok, new_state, :hibernate}`
      -  `:remove_handler`

    * `handle_call(msg, state)` - invoked when a `call/3` is done to a specific
      handler.

      It must return:

      -  `{:ok, reply, new_state}`
      -  `{:ok, reply, new_state, :hibernate}`
      -  `{:remove_handler, reply}`

    * `handle_info(msg, state)` - invoked to handle all other messages which
      are received by the process. Must return the same values as
      `handle_event/2`.

    * `terminate(reason, state)` - called when the event handler is removed or
      the event manager is terminating. It can return any term.

      The reason is one of:

      -  `:stop` - manager is terminating
      -  `{:stop, reason}` - monitored process terminated (for monitored handlers)
      -  `:remove_handler` - handler is being removed
      -  `{:error, term}` - handler crashed or returned a bad value
      -  `term` - any term passed to functions like `GenEvent.remove_handler/2`

    * `code_change(old_vsn, state, extra)` - called when the application
      code is being upgraded live (hot code swapping).

      It must return:

      -  `{:ok, new_state}`

  ## Name Registration

  A GenEvent is bound to the same name registration rules as a `GenServer`.
  Read more about it in the `GenServer` docs.

  ## Modes

  GenEvent stream supports three different notifications.

  On `GenEvent.ack_notify/2`, the manager acknowledges each event,
  providing back pressure, but processing of the message happens
  asynchronously.

  On `GenEvent.sync_notify/2`, the manager acknowledges an event
  just after it was processed by all event handlers.

  On `GenEvent.notify/2`, all events are processed asynchronously and
  there is no ack (which means there is no backpressure).

  ## Streaming

  `GenEvent` messages can be streamed with the help of `stream/2`.
  Here are some examples:

      stream = GenEvent.stream(pid)

      # Discard the next 10 events
      _ = Enum.drop(stream, 10)

      # Print all remaining events
      for event &lt;- stream do
        IO.inspect event
      end

  ## Learn more and compatibility

  If you wish to find out more about gen events, Elixir getting started
  guides provide a tutorial-like introduction. The documentation and links
  in Erlang can also provide extra insight.

    * http://elixir-lang.org/getting_started/mix_otp/1.html
    * http://www.erlang.org/doc/man/gen_event.html
    * http://learnyousomeerlang.com/event-handlers

  Keep in mind though Elixir and Erlang gen events are not 100% compatible.
  The `:gen_event.add_sup_handler/3` is not supported by Elixir&#39;s GenEvent,
  which in turn supports `GenEvent.add_mon_handler/3`.

  The benefits of the monitoring approach are described in the &quot;</span><span class="uppercaseAtom">Don</span><span class="charlist">&#39;t drink
  too much kool aid&quot; section of the &quot;Learn you some Erlang&quot; link above. Due
  to those changes, Elixir&#39;</span><span class="generic">s</span><span class="generic"> </span><span class="uppercaseAtom">GenEvent</span><span class="generic"> </span><span class="generic">does</span><span class="generic"> </span><span class="reservedWord">not</span><span class="generic"> </span><span class="generic">trap</span><span class="generic"> </span><span class="generic">exits</span><span class="generic"> </span><span class="generic">by</span><span class="generic"> </span><span class="generic">default</span><span class="generic">.</span><span class="generic">
</span><span class="generic">
</span><span class="generic"> </span><span class="generic"> </span><span class="uppercaseAtom">Futhermore</span><span class="generic">,</span><span class="generic"> </span><span class="uppercaseAtom">Elixir</span><span class="charlist">&#39;s also normalizes the `{:error, _}` tuples returned
  by many functions, in order to be more consistent with themselves and
  the `GenServer` module.
  &quot;&quot;&quot;

  @typedoc &quot;Return values of `start*` functions&quot;
  @type on_start :: {:ok, pid} | {:error, {:already_started, pid}}

  @typedoc &quot;The GenEvent manager name&quot;
  @type name :: atom | {:global, term} | {:via, module, term}

  @typedoc &quot;Options used by the `start*` functions&quot;
  @type options :: [name: name]

  @typedoc &quot;The event manager reference&quot;
  @type manager :: pid | name | {atom, node}

  @typedoc &quot;Supported values for new handlers&quot;
  @type handler :: atom | {atom, term} | {pid, reference}

  @doc false
  defmacro __using__(_) do
    quote location: :keep do
      @behaviour :gen_event

      @doc false
      def init(args) do
        {:ok, args}
      end

      @doc false
      def handle_event(_event, state) do
        {:ok, state}
      end

      @doc false
      def handle_call(msg, state) do
        # We do this to trick dialyzer to not complain about non-local returns.
        case :random.uniform(1) do
          1 -&gt; exit({:bad_call, msg})
          2 -&gt; {:remove_handler, :ok}
        end
      end

      @doc false
      def handle_info(_msg, state) do
        {:ok, state}
      end

      @doc false
      def terminate(_reason, _state) do
        :ok
      end

      @doc false
      def code_change(_old, state, _extra) do
        {:ok, state}
      end

      defoverridable [init: 1, handle_event: 2, handle_call: 2,
                      handle_info: 2, terminate: 2, code_change: 3]
    end
  end

  @spec start(options) :: on_start
  def start(options \\ []) when is_list(options) do
    do_start(:nolink, options)
  end

  @no_callback :&quot;no callback module&quot;

  defp do_start(mode, options) do
    case Keyword.get(options, :name) do
      nil -&gt;
        :gen.start(GenEvent, mode, @no_callback, [], [])
      atom when is_atom(atom) -&gt;
        :gen.start(GenEvent, mode, {:local, atom}, @no_callback, [], [])
      other when is_tuple(other) -&gt;
        :gen.start(GenEvent, mode, other, @no_callback, [], [])
    end
  end

  @spec stream(manager, Keyword.t) :: GenEvent.Stream.t
  def stream(manager, options \\ []) do
    %GenEvent.Stream{
      manager: manager,
      timeout: Keyword.get(options, :timeout, :infinity)}
  end

  @spec add_handler(manager, handler, term) :: :ok | {:error, term}
  def add_handler(manager, handler, args) do
    rpc(manager, {:add_handler, handler, args})
  end

  @spec add_mon_handler(manager, handler, term) :: :ok | {:error, term}
  def add_mon_handler(manager, handler, args) do
    rpc(manager, {:add_mon_handler, handler, args, self()})
  end

  @spec notify(manager, term) :: :ok
  def notify(manager, event)

  def notify({:global, name}, msg) do
    try do
      :global.send(name, {:notify, msg})
      :ok
    catch
      _, _ -&gt; :ok
    end
  end

  def notify({:via, mod, name}, msg) do
    try do
      mod.send(name, {:notify, msg})
      :ok
    catch
      _, _ -&gt; :ok
    end
  end

  def notify(other, msg) do
    send(other, {:notify, msg})
    :ok
  end

  @spec sync_notify(manager, term) :: :ok
  def sync_notify(manager, event) do
    rpc(manager, {:sync_notify, event})
  end

  @spec ack_notify(manager, term) :: :ok
  def ack_notify(manager, event) do
    rpc(manager, {:ack_notify, event})
  end

  @spec call(manager, handler, term, timeout) ::  term | {:error, term}
  def call(manager, handler, request, timeout \\ 5000) do
    try do
      :gen.call(manager, self(), {:call, handler, request}, timeout)
    catch
      :exit, reason -&gt;
        exit({reason, {__MODULE__, :call, [manager, handler, request, timeout]}})
    else
      {:ok, res} -&gt; res
    end
  end

  @spec remove_handler(manager, handler, term) :: term | {:error, term}
  def remove_handler(manager, handler, args) do
    rpc(manager, {:delete_handler, handler, args})
  end


  @spec swap_handler(manager, handler, term, handler, term) :: :ok | {:error, term}
  def swap_handler(manager, handler1, args1, handler2, args2) do
    rpc(manager, {:swap_handler, handler1, args1, handler2, args2})
  end

  @spec swap_mon_handler(manager, handler, term, handler, term) :: :ok | {:error, term}
  def swap_mon_handler(manager, handler1, args1, handler2, args2) do
    rpc(manager, {:swap_mon_handler, handler1, args1, handler2, args2, self()})
  end

  @spec which_handlers(manager) :: [handler]
  def which_handlers(manager) do
    rpc(manager, :which_handlers)
  end

  @spec stop(manager) :: :ok
  def stop(manager) do
    rpc(manager, :stop)
  end

  defp rpc(module, cmd) do
    # TODO: Change the tag once patch is accepted by OTP
    {:ok, reply} = :gen.call(module, self(), cmd, :infinity)
    reply
  end

  ## Init callbacks

  require Record
  Record.defrecordp :handler, [:module, :id, :state, :pid, :ref]

  @doc false
  def init_it(starter, :self, name, mod, args, options) do
    init_it(starter, self(), name, mod, args, options)
  end

  def init_it(starter, parent, name, _, _, options) do
    Process.put(:&quot;$initial_call&quot;, {__MODULE__, :init_it, 6})
    debug = :gen.debug_options(options)
    :proc_lib.init_ack(starter, {:ok, self()})
    loop(parent, name(name), [], debug, false)
  end

  @doc false
  def init_hib(parent, name, handlers, debug) do
    fetch_msg(parent, name, handlers, debug, true)
  end

  defp name({:local, name}),  do: name
  defp name({:global, name}), do: name
  defp name({:via, _, name}), do: name
  defp name(pid) when is_pid(pid), do: pid

  ## Loop

  defp loop(parent, name, handlers, debug, true) do
    :proc_lib.hibernate(__MODULE__, :init_hib, [parent, name, handlers, debug])
  end

  defp loop(parent, name, handlers, debug, false) do
    fetch_msg(parent, name, handlers, debug, false)
  end

  defp fetch_msg(parent, name, handlers, debug, hib) do
    receive do
      {:system, from, req} -&gt;
        :sys.handle_system_msg(req, from, parent, __MODULE__,
          debug, [name, handlers, hib], hib)
      {:EXIT, ^parent, reason} -&gt;
        server_terminate(reason, parent, handlers, name)
      msg when debug == [] -&gt;
        handle_msg(msg, parent, name, handlers, [])
      msg -&gt;
        debug = :sys.handle_debug(debug, &amp;print_event/3, name, {:in, msg})
        handle_msg(msg, parent, name, handlers, debug)
    end
  end

  defp handle_msg(msg, parent, name, handlers, debug) do
    case msg do
      {:notify, event} -&gt;
        {hib, handlers} = server_event(:async, event, handlers, name)
        loop(parent, name, handlers, debug, hib)
      {_from, _tag, {:notify, event}} -&gt;
        {hib, handlers} = server_event(:async, event, handlers, name)
        loop(parent, name, handlers, debug, hib)
      {_from, tag, {:ack_notify, event}} -&gt;
        reply(tag, :ok)
        {hib, handlers} = server_event(:ack, event, handlers, name)
        loop(parent, name, handlers, debug, hib)
      {_from, tag, {:sync_notify, event}} -&gt;
        {hib, handlers} = server_event(:sync, event, handlers, name)
        reply(tag, :ok)
        loop(parent, name, handlers, debug, hib)
      {:DOWN, ref, :process, _pid, reason} = other -&gt;
        case handle_down(ref, reason, handlers, name) do
          {:ok, handlers} -&gt;
            loop(parent, name, handlers, debug, false)
          :error -&gt;
            {hib, handlers} = server_info(other, handlers, name)
            loop(parent, name, handlers, debug, hib)
        end
      {_from, tag, {:call, handler, query}} -&gt;
        {hib, reply, handlers} = server_call(handler, query, handlers, name)
        reply(tag, reply)
        loop(parent, name, handlers, debug, hib)
      {_from, tag, {:add_handler, handler, args}} -&gt;
        {hib, reply, handlers} = server_add_handler(handler, args, handlers)
        reply(tag, reply)
        loop(parent, name, handlers, debug, hib)
      {_from, tag, {:add_mon_handler, handler, args, notify}} -&gt;
        {hib, reply, handlers} = server_add_mon_handler(handler, args, handlers, notify)
        reply(tag, reply)
        loop(parent, name, handlers, debug, hib)
      {_from, tag, {:add_process_handler, pid, notify}} -&gt;
        {hib, reply, handlers} = server_add_process_handler(pid, handlers, notify)
        reply(tag, reply)
        loop(parent, name, handlers, debug, hib)
      {_from, tag, {:delete_handler, handler, args}} -&gt;
        {reply, handlers} = server_remove_handler(handler, args, handlers, name)
        reply(tag, reply)
        loop(parent, name, handlers, debug, false)
      {_from, tag, {:swap_handler, handler1, args1, handler2, args2}} -&gt;
        {hib, reply, handlers} = server_swap_handler(handler1, args1, handler2, args2, handlers, nil, name)
        reply(tag, reply)
        loop(parent, name, handlers, debug, hib)
      {_from, tag, {:swap_mon_handler, handler1, args1, handler2, args2, mon}} -&gt;
        {hib, reply, handlers} = server_swap_handler(handler1, args1, handler2, args2, handlers, mon, name)
        reply(tag, reply)
        loop(parent, name, handlers, debug, hib)
      {_from, tag, :stop} -&gt;
        try do
          server_terminate(:normal, parent, handlers, name)
        catch
          :exit, :normal -&gt; :ok
        end
        reply(tag, :ok)
      {_from, tag, :which_handlers} -&gt;
        reply(tag, server_which_handlers(handlers))
        loop(parent, name, handlers, debug, false)
      {_from, tag, :get_modules} -&gt;
        reply(tag, server_get_modules(handlers))
        loop(parent, name, handlers, debug, false)
      other -&gt;
        {hib, handlers} = server_info(other, handlers, name)
        loop(parent, name, handlers, debug, hib)
    end
  end

  ## System callbacks

  @doc false
  def system_continue(parent, debug, [name, handlers, hib]) do
    loop(parent, name, handlers, debug, hib)
  end

  @doc false
  def system_terminate(reason, parent, _debug, [name, handlers, _hib]) do
    server_terminate(reason, parent, handlers, name)
  end

  @doc false
  def system_code_change([name, handlers, hib], module , old_vsn, extra) do
    handlers =
      for handler &lt;- handlers do
        if handler(handler, :module) == module do
          {:ok, state} = module.code_change(old_vsn, handler(handler, :state), extra)
          handler(handler, state: state)
        else
          handler
        end
      end
    {:ok, [name, handlers, hib]}
  end

  @doc false
  def system_get_state([_name, handlers, _hib]) do
    tuples = for handler(module: mod, id: id, state: state) &lt;- handlers do
      {mod, id, state}
    end
    {:ok, tuples}
  end

  @doc false
  def system_replace_state(fun, [name, handlers, hib]) do
    {handlers, states} =
      :lists.unzip(for handler &lt;- handlers do
        handler(module: mod, id: id, state: state) = handler
        cur = {mod, id, state}
        try do
          new = {^mod, ^id, new_state} = fun.(cur)
          {handler(handler, state: new_state), new}
        catch
          _, _ -&gt;
            {handler, cur}
        end
      end)
    {:ok, states, [name, handlers, hib]}
  end

  @doc false
  def format_status(opt, status_data) do
    [pdict, sys_state, parent, _debug, [name, handlers, _hib]] = status_data
    header = :gen.format_status_header(&#39;</span><span class="uppercaseAtom">Status</span><span class="generic"> </span><span class="generic">for</span><span class="generic"> </span><span class="generic">event</span><span class="generic"> </span><span class="generic">handler</span><span class="charlist">&#39;, name)

    formatted = for handler &lt;- handlers do
      handler(module: module, state: state) = handler
      if function_exported?(module, :format_status, 2) do
        try do
          state = module.format_status(opt, [pdict, state])
          handler(handler, state: state)
        catch
          _, _ -&gt; handler
        end
      else
        handler
      end
    end

    [header: header,
     data: [{&#39;</span><span class="uppercaseAtom">Status</span><span class="charlist">&#39;, sys_state}, {&#39;</span><span class="uppercaseAtom">Parent</span><span class="charlist">&#39;, parent}],
     items: {&#39;</span><span class="uppercaseAtom">Installed</span><span class="generic"> </span><span class="generic">handlers</span><span class="charlist">&#39;, formatted}]
  end

  ## Loop helpers

  defp print_event(dev, {:in, msg}, name) do
    case msg do
      {:notify, event} -&gt;
        IO.puts dev, &quot;*DBG* #{inspect name} got event #{inspect event}&quot;
      {_, _, {:call, handler, query}} -&gt;
        IO.puts dev, &quot;*DBG* #{inspect name} (handler #{inspect handler}) got call #{inspect query}&quot;
      _ -&gt;
        IO.puts dev, &quot;*DBG* #{inspect name} got #{inspect msg}&quot;
    end
  end

  defp print_event(dev, dbg, name) do
    IO.puts dev, &quot;*DBG* #{inspect name}: #{inspect dbg}&quot;
  end

  defp server_add_handler({module, id}, args, handlers) do
    handler = handler(module: module, id: {module, id})
    do_add_handler(module, handler, args, handlers, :ok)
  end

  defp server_add_handler(module, args, handlers) do
    handler = handler(module: module, id: module)
    do_add_handler(module, handler, args, handlers, :ok)
  end

  defp server_add_mon_handler({module, id}, args, handlers, notify) do
    ref = Process.monitor(notify)
    handler = handler(module: module, id: {module, id}, pid: notify, ref: ref)
    do_add_handler(module, handler, args, handlers, :ok)
  end

  defp server_add_mon_handler(module, args, handlers, notify) do
    ref = Process.monitor(notify)
    handler = handler(module: module, id: module, pid: notify, ref: ref)
    do_add_handler(module, handler, args, handlers, :ok)
  end

  defp server_add_process_handler(pid, handlers, notify) do
    ref = Process.monitor(pid)
    handler = handler(module: GenEvent.Stream, id: {self(), ref},
                      pid: notify, ref: ref)
    do_add_handler(GenEvent.Stream, handler, {pid, ref}, handlers, {self(), ref})
  end

  defp server_remove_handler(module, args, handlers, name) do
    do_take_handler(module, args, handlers, name, :remove, :normal)
  end

  defp server_swap_handler(module1, args1, module2, args2, handlers, sup, name) do
    {state, handlers} =
      do_take_handler(module1, args1, handlers, name, :swapped, {:swapped, module2, sup})

    if sup do
      server_add_mon_handler(module2, {args2, state}, handlers, sup)
    else
      server_add_handler(module2, {args2, state}, handlers)
    end
  end

  defp server_info(event, handlers, name) do
    handlers = :lists.reverse(handlers)
    server_notify(event, :handle_info, handlers, name, handlers, [], false)
  end

  defp server_event(mode, event, handlers, name) do
    {handlers, streams} = server_split_process_handlers(mode, event, handlers, [], [])
    {hib, handlers} = server_notify(event, :handle_event, handlers, name, handlers, [], false)
    {hib, server_collect_process_handlers(mode, event, streams, handlers, name)}
  end

  defp server_split_process_handlers(mode, event, [handler|t], handlers, streams) do
    case handler(handler, :id) do
      {pid, _ref} when is_pid(pid) -&gt;
        server_process_notify(mode, event, handler)
        server_split_process_handlers(mode, event, t, handlers, [handler|streams])
      _ -&gt;
        server_split_process_handlers(mode, event, t, [handler|handlers], streams)
    end
  end

  defp server_split_process_handlers(_mode, _event, [], handlers, streams) do
    {handlers, streams}
  end

  defp server_process_notify(mode, event, handler(state: {pid, ref})) do
    send pid, {self(), {self(), ref}, {mode_to_tag(mode), event}}
  end

  defp mode_to_tag(:ack),   do: :ack_notify
  defp mode_to_tag(:sync),  do: :sync_notify
  defp mode_to_tag(:async), do: :notify

  defp server_notify(event, fun, [handler|t], name, handlers, acc, hib) do
    case server_update(handler, fun, event, name, handlers) do
      {new_hib, handler} -&gt;
        server_notify(event, fun, t, name, handlers, [handler|acc], hib or new_hib)
      :error -&gt;
        server_notify(event, fun, t, name, handlers, acc, hib)
    end
  end

  defp server_notify(_, _, [], _, _, acc, hib) do
    {hib, acc}
  end

  defp server_update(handler, fun, event, name, _handlers) do
    handler(module: module, state: state) = handler

    case do_handler(module, fun, [event, state]) do
      {:ok, res} -&gt;
        case res do
          {:ok, state} -&gt;
            {false, handler(handler, state: state)}
          {:ok, state, :hibernate} -&gt;
            {true, handler(handler, state: state)}
          :remove_handler -&gt;
            do_terminate(handler, :remove_handler, event, name, :normal)
            :error
          other -&gt;
            reason = {:bad_return_value, other}
            do_terminate(handler, {:error, reason}, event, name, reason)
            :error
        end
      {:error, reason} -&gt;
        do_terminate(handler, {:error, reason}, event, name, reason)
        :error
    end
  end

  defp server_collect_process_handlers(:async, event, [handler|t], handlers, name) do
    server_collect_process_handlers(:async, event, t, [handler|handlers], name)
  end

  defp server_collect_process_handlers(mode, event, [handler|t], handlers, name) when mode in [:sync, :ack] do
    handler(ref: ref, id: id) = handler

    receive do
      {^ref, :ok} -&gt;
        server_collect_process_handlers(mode, event, t, [handler|handlers], name)
      {_from, tag, {:delete_handler, ^id, args}} -&gt;
        do_terminate(handler, args, :remove, name, :normal)
        reply(tag, :ok)
        server_collect_process_handlers(mode, event, t, handlers, name)
      {:DOWN, ^ref, _, _, reason} -&gt;
        do_terminate(handler, {:stop, reason}, :DOWN, name, :shutdown)
        server_collect_process_handlers(mode, event, t, handlers, name)
    end
  end

  defp server_collect_process_handlers(_mode, _event, [], handlers, _name) do
    handlers
  end

  defp server_call(module, query, handlers, name) do
    case :lists.keyfind(module, handler(:id) + 1, handlers) do
      false -&gt;
        {false, {:error, :not_found}, handlers}
      handler -&gt;
        case server_call_update(handler, query, name, handlers) do
          {{hib, handler}, reply} -&gt;
            {hib, reply, :lists.keyreplace(module, handler(:id) + 1, handlers, handler)}
          {:error, reply} -&gt;
            {false, reply, :lists.keydelete(module, handler(:id) + 1, handlers)}
        end
    end
  end

  defp server_call_update(handler, query, name, _handlers) do
    handler(module: module, state: state) = handler
    case do_handler(module, :handle_call, [query, state]) do
      {:ok, res} -&gt;
        case res do
          {:ok, reply, state} -&gt;
            {{false, handler(handler, state: state)}, reply}
          {:ok, reply, state, :hibernate} -&gt;
            {{true, handler(handler, state: state)}, reply}
          {:remove_handler, reply} -&gt;
            do_terminate(handler, :remove_handler, query, name, :normal)
            {:error, reply}
          other -&gt;
            reason = {:bad_return_value, other}
            do_terminate(handler, {:error, reason}, query, name, reason)
            {:error, {:error, reason}}
        end
      {:error, reason} -&gt;
        do_terminate(handler, {:error, reason}, query, name, reason)
        {:error, {:error, reason}}
    end
  end

  defp server_get_modules(handlers) do
    (for handler(module: module) &lt;- handlers, do: module)
    |&gt; :ordsets.from_list
    |&gt; :ordsets.to_list
  end

  defp server_which_handlers(handlers) do
    for handler(id: id) &lt;- handlers, do: id
  end

  defp server_terminate(reason, _parent, handlers, name) do
    _ =
      for handler &lt;- handlers do
        do_terminate(handler, :stop, :stop, name, :shutdown)
      end
    exit(reason)
  end

  defp reply({from, ref}, msg) do
    send from, {ref, msg}
  end

  defp handle_down(ref, reason, handlers, name) do
    case :lists.keyfind(ref, handler(:ref) + 1, handlers) do
      false -&gt; :error
      handler -&gt;
        do_terminate(handler, {:stop, reason}, :DOWN, name, :shutdown)
        {:ok, :lists.keydelete(ref, handler(:ref) + 1, handlers)}
    end
  end

  defp do_add_handler(module, handler, arg, handlers, succ) do
    case :lists.keyfind(handler(handler, :id), handler(:id) + 1, handlers) do
      false -&gt;
        case do_handler(module, :init, [arg]) do
          {:ok, res} -&gt;
            case res do
              {:ok, state} -&gt;
                {false, succ, [handler(handler, state: state)|handlers]}
              {:ok, state, :hibernate} -&gt;
                {true, succ, [handler(handler, state: state)|handlers]}
              {:error, _} = error -&gt;
                {false, error, handlers}
              other -&gt;
                {false, {:error, {:bad_return_value, other}}, handlers}
            end
          {:error, _} = error -&gt;
            {false, error, handlers}
        end
      _ -&gt;
        {false, {:error, :already_present}, handlers}
    end
  end

  defp do_take_handler(module, args, handlers, name, last_in, reason) do
    case :lists.keytake(module, handler(:id) + 1, handlers) do
      {:value, handler, handlers} -&gt;
        {do_terminate(handler, args, last_in, name, reason), handlers}
      false -&gt;
        {{:error, :not_found}, handlers}
    end
  end

  defp do_terminate(handler, arg, last_in, name, reason) do
    handler(module: module, state: state) = handler

    res =
      case do_handler(module, :terminate, [arg, state]) do
        {:ok, res} -&gt; res
        {:error, _} = error -&gt; error
      end
    report_terminate(handler, reason, state, last_in, name)
    res
  end

  defp do_handler(mod, fun, args) do
    try do
      apply(mod, fun, args)
    catch
      :throw, val -&gt; {:ok, val}
      :error, val -&gt; {:error, {val, System.stacktrace}}
      :exit, val  -&gt; {:error, val}
    else
      res -&gt; {:ok, res}
    end
  end

  defp report_terminate(handler, reason, state, last_in, name) do
    report_error(handler, reason, state, last_in, name)
    if ref = handler(handler, :ref) do
      Process.demonitor(ref, [:flush])
    end
    if pid = handler(handler, :pid) do
      send pid, {:gen_event_EXIT, handler(handler, :id), reason}
    end
  end

  defp report_error(_handler, :normal, _, _, _), do: :ok
  defp report_error(_handler, :shutdown, _, _, _), do: :ok
  defp report_error(_handler, {:swapped, _, _}, _, _, _), do: :ok
  defp report_error(handler, reason, state, last_in, name) do
    reason =
      case reason do
        {:undef, [{m,f,a,_}|_]=mfas} -&gt;
          cond do
            :code.is_loaded(m) -&gt;
              {:&quot;module could not be loaded&quot;, mfas}
            function_exported?(m, f, length(a)) -&gt;
              reason
            true -&gt;
              {:&quot;function not exported&quot;, mfas}
          end
        _ -&gt;
          reason
      end

    formatted = report_status(handler, state)

    :error_logger.error_msg(
      &#39;</span><span class="generic">*</span><span class="generic">*</span><span class="generic"> </span><span class="generic">gen_event</span><span class="generic"> </span><span class="generic">handler</span><span class="generic"> </span><span class="generic">~</span><span class="generic">p</span><span class="generic"> </span><span class="generic">crashed</span><span class="generic">.</span><span class="generic">~</span><span class="generic">n</span><span class="charlist">&#39; ++
      &#39;</span><span class="generic">*</span><span class="generic">*</span><span class="generic"> </span><span class="uppercaseAtom">Was</span><span class="generic"> </span><span class="generic">installed</span><span class="generic"> </span><span class="reservedWord">in</span><span class="generic"> </span><span class="generic">~</span><span class="generic">p</span><span class="generic">~</span><span class="generic">n</span><span class="charlist">&#39; ++
      &#39;</span><span class="generic">*</span><span class="generic">*</span><span class="generic"> </span><span class="uppercaseAtom">Last</span><span class="generic"> </span><span class="generic">event</span><span class="generic"> </span><span class="atom">was:</span><span class="generic"> </span><span class="generic">~</span><span class="generic">p</span><span class="generic">~</span><span class="generic">n</span><span class="charlist">&#39; ++
      &#39;</span><span class="generic">*</span><span class="generic">*</span><span class="generic"> </span><span class="uppercaseAtom">When</span><span class="generic"> </span><span class="generic">handler</span><span class="generic"> </span><span class="generic">state</span><span class="generic"> </span><span class="generic">==</span><span class="generic"> </span><span class="generic">~</span><span class="generic">p</span><span class="generic">~</span><span class="generic">n</span><span class="charlist">&#39; ++
      &#39;</span><span class="generic">*</span><span class="generic">*</span><span class="generic"> </span><span class="uppercaseAtom">Reason</span><span class="generic"> </span><span class="generic">==</span><span class="generic"> </span><span class="generic">~</span><span class="generic">p</span><span class="generic">~</span><span class="generic">n</span><span class="generic">&#39;</span><span class="generic">,</span><span class="generic"> </span><span class="delimiter">[</span><span class="generic">handler</span><span class="delimiter">(</span><span class="generic">handler</span><span class="generic">,</span><span class="generic"> </span><span class="atom">:id</span><span class="delimiter">)</span><span class="generic">,</span><span class="generic"> </span><span class="generic">name</span><span class="generic">,</span><span class="generic"> </span><span class="generic">last_in</span><span class="generic">,</span><span class="generic"> </span><span class="generic">formatted</span><span class="generic">,</span><span class="generic"> </span><span class="generic">reason</span><span class="delimiter">]</span><span class="delimiter">)</span><span class="generic">
</span><span class="generic"> </span><span class="generic"> </span><span class="reservedWord">end</span><span class="generic">
</span><span class="generic">
</span><span class="generic"> </span><span class="generic"> </span><span class="reservedWord">defp</span><span class="generic"> </span><span class="generic">report_status</span><span class="delimiter">(</span><span class="generic">handler</span><span class="delimiter">(</span><span class="atom">module:</span><span class="generic"> </span><span class="generic">module</span><span class="delimiter">)</span><span class="generic">,</span><span class="generic"> </span><span class="generic">state</span><span class="delimiter">)</span><span class="generic"> </span><span class="reservedWord">do</span><span class="generic">
</span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="reservedWord">if</span><span class="generic"> </span><span class="generic">function_exported?</span><span class="delimiter">(</span><span class="generic">module</span><span class="generic">,</span><span class="generic"> </span><span class="atom">:format_status</span><span class="generic">,</span><span class="generic"> </span><span class="integer">2</span><span class="delimiter">)</span><span class="generic"> </span><span class="reservedWord">do</span><span class="generic">
</span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="reservedWord">try</span><span class="generic"> </span><span class="reservedWord">do</span><span class="generic">
</span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic">module</span><span class="generic">.</span><span class="generic">format_status</span><span class="delimiter">(</span><span class="atom">:terminate</span><span class="generic">,</span><span class="generic"> </span><span class="delimiter">[</span><span class="uppercaseAtom">Process</span><span class="generic">.</span><span class="generic">get</span><span class="delimiter">(</span><span class="delimiter">)</span><span class="generic">,</span><span class="generic"> </span><span class="generic">state</span><span class="delimiter">]</span><span class="delimiter">)</span><span class="generic">
</span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="reservedWord">catch</span><span class="generic">
</span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="unusedVariable">_</span><span class="generic">,</span><span class="generic"> </span><span class="unusedVariable">_</span><span class="generic"> </span><span class="generic">-</span><span class="generic">&gt;</span><span class="generic"> </span><span class="generic">state</span><span class="generic">
</span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="reservedWord">end</span><span class="generic">
</span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="reservedWord">else</span><span class="generic">
</span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic">state</span><span class="generic">
</span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="generic"> </span><span class="reservedWord">end</span><span class="generic">
</span><span class="generic"> </span><span class="generic"> </span><span class="reservedWord">end</span><span class="generic">
</span><span class="reservedWord">end</span><span class="generic">
</span></code>
</pre>
</body>
</html>