#!/usr/bin/env elixir

defmodule Neurone do

  def train_multiple neurone, [] do
    neurone
  end

  def train_multiple neurone, training do
    {input, output} = hd(training)
    train_multiple(train(neurone, input, output), tl(training))
  end

  def train neurone, input, output do
    neurone = neurone ++ [{input, output}]
    neurone
  end

  def try neurone, input, forward_to_call, backward_to_call do
    # some work there with input
    output = forward_to_call.(neurone, input)
    neurone = neurone ++ [{input, output}]
    {neurone, output}
  end

end

defmodule NeuralBinary do
  def forward n, i do
    elem hd(Enum.filter(n, fn k -> {k,_} = k; k == i end)), 1
  end
end

n_xor = Neurone.train_multiple([], [{[1,1],0}, {[0,0],0}, {[1,0],1}, {[0,1],1}])

r1 = Neurone.try(n_xor, [1, 1], &NeuralBinary.forward/2, nil)
n_xor = elem r1, 0
r1 = elem r1, 1

r2 = Neurone.try(n_xor, [0, 0], &NeuralBinary.forward/2, nil)
n_xor = elem r2, 0
r2 = elem r2, 1

r3 = Neurone.try(n_xor, [1, 0], &NeuralBinary.forward/2, nil)
n_xor = elem r3, 0
r3 = elem r3, 1

r4 = Neurone.try(n_xor, [0, 1], &NeuralBinary.forward/2, nil)
n_xor = elem r4, 0
r4 = elem r4, 1

IO.puts "1 ^ 1 = #{Integer.to_string r1}"
IO.puts "0 ^ 0 = #{Integer.to_string r2}"
IO.puts "1 ^ 0 = #{Integer.to_string r3}"
IO.puts "0 ^ 1 = #{Integer.to_string r4}"
