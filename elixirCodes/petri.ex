defmodule Petri do
  defstruct pre: [nil], v: nil, pos: [nil]
  def netM do
    [
      %Petri{v: :P0, pos: [:A]},
      %Petri{pre: [:P0], v: :A, pos: [:P1,:P2]},
      %Petri{pre: [:A], v: :P1, pos: [:B,:D]},
      %Petri{pre: [:A], v: :P2, pos: [:C,:D]},
      %Petri{pre: [:P1], v: :B, pos: [:P3]},
      %Petri{pre: [:P2], v: :C, pos: [:P4]},
      %Petri{pre: [:P1,:P2], v: :D, pos: [:P3,:P4]},
      %Petri{pre: [:B,:D], v: :P3, pos: [:E]},
      %Petri{pre: [:C,:D], v: :P4, pos: [:E]},
      %Petri{pre: [:P3,:P4], v: :E, pos: [:P5]},
      %Petri{pre: [:E], v: :P5}
    ]
  end

  def netM2 do
    [
      %Petri{v: :P0, pos: [:A]},
      %Petri{pre: [:P0], v: :A, pos: [:P1,:P2]},
      %Petri{pre: [:A], v: :P1, pos: [:B]},
      %Petri{pre: [:A,:D], v: :P2, pos: [:C]},
      %Petri{pre: [:P1], v: :B, pos: [:P3]},
      %Petri{pre: [:P2], v: :C, pos: [:P4]},
      %Petri{pre: [:P4], v: :D, pos: [:P2]},
      %Petri{pre: [:B], v: :P3, pos: [:E]},
      %Petri{pre: [:C], v: :P4, pos: [:D,:E]},
      %Petri{pre: [:P3,:P4], v: :E, pos: [:P5]},
      %Petri{pre: [:E], v: P5}
    ]
  end

  def netM3 do
    [
      %Petri{v: :P0, pos: [:A]},
      %Petri{pre: [:P0], v: :A, pos: [:P1,:P2]},
      %Petri{pre: [:A], v: :P1, pos: [:B,:D]},
      %Petri{pre: [:A], v: :P2, pos: [:C,:D]},
      %Petri{pre: [:P1], v: :B, pos: [:P3]},
      %Petri{pre: [:P2], v: :C, pos: [:P4]},
      %Petri{pre: [:P1,:P2], v: :D, pos: [:P3]},
      %Petri{pre: [:B,:D], v: :P3, pos: [:E]},
      %Petri{pre: [:C], v: :P4, pos: [:E]},
      %Petri{pre: [:P3,:P4], v: :E, pos: [:P5]},
      %Petri{pre: [:E], v: P5}
    ]
  end

  def netL do
    [
      [:P0,:A],
      [:A,:P1],
      [:A,:P2],
      [:P1,:B],
      [:P1,:D],
      [:P2,:C],
      [:P2,:D],
      [:B,:P3],
      [:C,:P4],
      [:D,:P3],
      [:D,:P4],
      [:P3,:E],
      [:P4,:E],
      [:E,:P5]
    ]
  end

  def netL2 do
    [
      [:P0,:A],
      [:A,:P1],
      [:A,:P2],
      [:P1,:B],
      [:P2,:C],
      [:B,:P3],
      [:C,:P4],
      [:D,:P2],
      [:P3,:E],
      [:P4,:D],
      [:P4,:E],
      [:E,:P5]
    ]
  end

  def netL3 do
    [
      [:P0,:A],
      [:A,:P1],
      [:A,:P2],
      [:P1,:B],
      [:P1,:D],
      [:P2,:C],
      [:P2,:D],
      [:B,:P3],
      [:C,:P4],
      [:D,:P3],
      [:P3,:E],
      [:P4,:E],
      [:E,:P5]
    ]
  end

  def m0, do: [:P0]
  def m1, do: [:P1,:P2]
  def m2, do: [:P3,:P4]
  def m3, do: [:P5]
  def m4, do: [:P0,:P1]
  def m5, do: [:P1,:P2,:P4]
  def m6, do: [:P2,:P3]
  def m7, do: [:P4]
end

defmodule AdhocFunctions do
  def find([head|_rest],elem) when head.v == elem, do: head
  def find([_head|rest],elem), do: find(rest,elem)
  def find([],_elem), do: %Petri{}

  def fire(net,m,t) do
    node = find(net,t)
    if (node.pre -- m) == [] do
      Enum.sort(Enum.uniq(node.pos ++ (m -- node.pre)))
    else
      m
    end
  end

  def enablement([head|rest],m) do
    if ((head.pre -- m) == []) do
      [head.v] ++ enablement(rest,m)
    else
      enablement(rest,m)
    end
  end
  def enablement([],_m), do: []

  def checkLine(_net,_m,[]), do: 1
  def checkLine(net,m,[head|rest]) do
    newMarking = fire(net,m,String.to_atom(head))
    if newMarking != m do
      checkLine(net,newMarking,rest)
    else
      0
    end
  end

  def checkAllList(_net,_m,[]), do: 0
  def checkAllList(net,m,[head|rest]), do: checkLine(net,m,head) + checkAllList(net,m,rest)

  def replay(net,m0,fileName) do
    lista =
    File.read!(fileName)
    |> String.split
    |> Enum.map(fn l -> String.split(l, ",") end)
    n = checkAllList(net,m0,lista)
    %{reejecutadas: n, noReejecutadas: length(lista) - n}
  end

  def links(_net,_m,_prev,[]), do: []
  def links(net,m,prev,[head|rest]) do
    newMarking = fire(net,m,head)
    if(prev == newMarking) do
      Enum.uniq([[m,head,newMarking]] ++ links(net,m,prev,rest))
    else
      Enum.uniq([[m,head,newMarking]] ++ links(net,m,prev,rest) ++ links(net,newMarking,m,enablement(net,newMarking)))
    end
  end

  def reachability(_net,[]), do: []
  def reachability(net,m) do
    links(net,m,[],enablement(net,m))
  end
end

defmodule PairFunctions do
  def findPre([head|rest],elem) when hd(tl(head)) == elem, do: [hd(head)] ++ findPre(rest,elem)
  def findPre([_head|rest],elem), do: findPre(rest,elem)
  def findPre([],_elem), do: []

  def findPos([head|rest],elem) when hd(head) == elem, do: [hd(tl(head))] ++ findPos(rest,elem)
  def findPos([_head|rest],elem), do: findPos(rest,elem)
  def findPos([],_elem), do: []

  def fire(net,m,t) do
    nodePre = findPre(net,t)
    if (nodePre -- m) == [] do
      Enum.sort(Enum.uniq(findPos(net,t) ++ (m -- nodePre)))
    else
      m
    end
  end

  def enablement([],_m), do: []
  def enablement([head|rest],m) do
    pre = findPre(Petri.netL,hd(head))
    if (pre != [] && (pre -- m) == []) do
      Enum.uniq([hd(head)] ++ enablement(rest,m))
    else
      enablement(rest,m)
    end
  end

  def checkLine(_net,_m,[]), do: 1
  def checkLine(net,m,[head|rest]) do
    newMarking = fire(net,m,String.to_atom(head))
    if newMarking != m do
      checkLine(net,newMarking,rest)
    else
      0
    end
  end

  def checkAllList(_net,_m,[]), do: 0
  def checkAllList(net,m,[head|rest]), do: checkLine(net,m,head) + checkAllList(net,m,rest)

  def replay(net,m0,fileName) do
    lista =
    File.read!(fileName)
    |> String.split
    |> Enum.map(fn l -> String.split(l, ",") end)
    n = checkAllList(net,m0,lista)
    %{reejecutadas: n, noReejecutadas: length(lista) - n}
  end

  def enablementSpecial([],_m,_net), do: []
  def enablementSpecial([head|rest],m,net) do
    pre = findPre(net,hd(head))
    if (pre != [] && (pre -- m) == []) do
      Enum.uniq([hd(head)] ++ enablementSpecial(rest,m,net))
    else
      enablementSpecial(rest,m,net)
    end
  end

  def links(_net,_m,_prev,[]), do: []
  def links(net,m,prev,[head|rest]) do
    newMarking = fire(net,m,head)
    if(prev == newMarking)do
      Enum.uniq([[m,head,newMarking]] ++ links(net,m,prev,rest))
    else
      Enum.uniq([[m,head,newMarking]] ++ links(net,m,prev,rest) ++ links(net,newMarking,m,enablementSpecial(net,newMarking,net)))
    end
  end

  def reachability(_net,[]), do: []
  def reachability(net,m) do
    links(net,m,[],enablementSpecial(net,m,net))
  end
end
