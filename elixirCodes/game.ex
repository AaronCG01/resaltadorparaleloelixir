defmodule Game do
  use GenServer

  def start_link(name \\ :name) do
    word = Dictionary.random_word() |> String.trim()
    GenServer.start_link(__MODULE__, word, name: name)
  end

  def init(word), do: {:ok, {word, "", "", 9}}

  def submit_guess(pid, guess), do: GenServer.cast(pid, {:submit_guess, guess})
  def get_feedback(pid), do: GenServer.call(pid, :get_feedback)

  def handle_cast({:submit_guess, letter}, state) do
    res = Hangman.score_guess(state, letter)
    {:noreply, res}
  end

  def handle_call(:get_feedback, _from, {_,_,_,turns} = state) do
    {
      :reply,
      %{
        feedback: Hangman.format_feedback(state),
        remaining_turns: turns,
        status: :playing
      },
      state
    }
  end

end
